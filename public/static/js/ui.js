$(function() {
    $(".parent ul").hide();

    $(".parent, .parent ul").hover(function () {
        $(this).children('ul').slideToggle(300);
    });


    $("#notifications").click(function () {
        $('#notifications_holder').show();
    });

    $(document).mouseup(function (e)
    {
        var container = $("#notifications_holder");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });

    $('.notification_title').click(function (e) {
        e.preventDefault();
        var notificationId = $(this).attr('data-id');
        var url = $(this).attr('href');
        $.post(
            "/set-read-notification",
            {id: notificationId},
            function(success){
                window.location.replace(url);
            }
        );
    });
});
<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace YonaCMS\Plugin;

use \Phalcon\Mvc\User\Plugin;

class Title extends Plugin
{

    public function __construct($di)
    {
        $helper = $di->get('helper');
        if (!$helper->meta()->get('seo-manager')) {
            $helper->title($helper->translate('SITE NAME'));
        }
    }

} 
<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace YonaCMS\Plugin;

use Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\User\Plugin,
    Phalcon\Mvc\View,
    Application\Acl\DefaultAcl;

class Acl extends Plugin
{

    public function __construct(DefaultAcl $acl, Dispatcher $dispatcher, View $view)
    {
        $role = $this->getRole();

        $module = $dispatcher->getModuleName();
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $resourceKey = $module . '/' . $controller;
        $resourceVal = $action;

        if ($acl->isResource($resourceKey)) {
            if (!$acl->isAllowed($role, $resourceKey, $resourceVal)) {
                $this->accessDenied($role, $module, $controller, $resourceKey, $resourceVal, $view);
            }
        } else {
            $this->resourceNotFound($module, $controller, $resourceKey, $view);
        }

    }

    private function getRole()
    {
        $auth = $this->session->get('auth');
        if (!$auth) {
            $role = 'guest';
        } else {
            if ($auth->admin_session == true) {
                $role = \Admin\Model\AdminUser::getRoleById($auth->id);
            } else {
                $role = 'member';
            }
        }
        return $role;
    }

    private function accessDenied($role, $module, $controller, $resourceKey = null, $resourceVal = null, View $view)
    {
        if($module == 'admin' || $controller == 'admin'){
            return $this->redirect('/admin/index');
        }
        else{
            return $this->redirect('/login');
        }
        /*
        $view->setViewsDir(__DIR__ . '/../modules/Index/views/');
        $view->setPartialsDir('');
        $view->message = "$role - Access Denied to resource <b>$resourceKey::$resourceVal</b>";
        $view->partial('error/error403');

        $response = new \Phalcon\Http\Response();
        $response->setHeader(403, 'Forbidden');
        $response->sendHeaders();
        echo $response->getContent();
        exit;*/
    }

    private function resourceNotFound($module, $controller, $resourceKey, View $view)
    {
        /*
        if($module == 'admin'){
            return $this->redirect('/admin/index');
        }
        else{
            return $this->redirect('/index');
        }

        return $this->redirect('/index');
        */

        $view->setViewsDir(__DIR__ . '/../modules/Index/views/');
        $view->setPartialsDir('');
        $view->message = "Acl resource <b>$resourceKey</b> in <b>/app/config/acl.php</b> not exists";
        $view->partial('error/error404');
        $response = new \Phalcon\Http\Response();
        $response->setHeader(404, 'Not Found');
        $response->sendHeaders();
        echo $response->getContent();
        exit;
    }

    private function redirect($url, $code = 302)
    {
        switch ($code) {
            case 301 :
                header('HTTP/1.1 301 Moved Permanently');
                break;
            case 302 :
                header('HTTP/1.1 302 Moved Temporarily');
                break;
        }
        header('Location: ' . $url);
        exit;
    }

}
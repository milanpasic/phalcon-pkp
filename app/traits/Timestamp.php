<?php

namespace Traits;

trait Timestamp
{
    public function beforeCreate()
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $this->created_at = $currentDateTime;
        $this->updated_at = $currentDateTime;
    }

    public function beforeUpdate()
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $this->updated_at = $currentDateTime;
    }
}
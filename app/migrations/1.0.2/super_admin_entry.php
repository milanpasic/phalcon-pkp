<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class SuperAdminEntryMigration_102
 */
class SuperAdminEntryMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {

    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        self::$_connection->insert(
            "admin_user",
            [
                1,
                'super_admin',
                'super_admin',
                'super_admin@phalcon-pkp.com',
                'Super Admin',
                '$2y$10$vTDXd/FKqYPfHBKXnQKsvO32kELyqnrueytrkQQpWYbmtRofFpaDS',
                1,
            ]
        );
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}

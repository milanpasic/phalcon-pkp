<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ConfigEntriesMigration_101
 */
class ConfigEntriesMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {

    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        self::$_connection->insert(
            "cms_configuration",
            [
                "DEBUG_MODE",
                1,
            ]
        );

        self::$_connection->insert(
            "cms_configuration",
            [
                "DISPLAY_CHANGELOG",
                1,
            ]
        );

        self::$_connection->insert(
            "cms_configuration",
            [
                "PROFILER",
                1,
            ]
        );

        self::$_connection->insert(
            "cms_configuration",
            [
                "TECHNICAL_WORKS",
                0,
            ]
        );

        self::$_connection->insert(
            "cms_configuration",
            [
                "ADMIN_EMAIL",
                'milanpasic92@gmail.com',
            ]
        );
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}

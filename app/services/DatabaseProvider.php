<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Services;

use Phalcon\Db\Adapter\Pdo\Mysql;

class DatabaseProvider {

    public static function getDbConnection(){
        $di = \Phalcon\Di::getDefault();
        $config = $di->get('config');

        $config = [
            "host"     => $config->database->host,
            "username" => $config->database->username,
            "password" => $config->database->password,
            "dbname"   => $config->database->dbname,
            "charset"  => $config->database->charset,
        ];

        $connection = new Mysql($config);
        $connection->connect();

        return $connection;
    }
}

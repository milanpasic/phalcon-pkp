<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Services;

class MailService {

    public function send($to, $subject, $message){
        $config = [
            'driver'     => 'smtp',
            'host'       => 'smtp.gmail.com',
            'port'       => 465,
            'encryption' => 'ssl',
            'username'   => SEND_FROM_EMAIL_USERNAME,
            'password'   => SEND_FROM_EMAIL_PASSWORD,
            'from'       => [
                'email' => SEND_FROM_EMAIL_USERNAME,
                'name'  => 'Phalcon PKP'
            ]
        ];

        $mailer = new \Phalcon\Ext\Mailer\Manager($config);

        $message = $mailer->createMessage()
            ->to($to)
            ->subject($subject)
            ->content($message);

        //$message->cc('example_cc@gmail.com');
        //$message->bcc('example_bcc@gmail.com');
        $message->send();
    }
}

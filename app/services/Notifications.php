<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Services;

use Phalcon\Db\Adapter\Pdo\Mysql;
use Services\DatabaseProvider;
use Member\Model\Notification;

class Notifications {

    /*
     * Notify entity example
     */
    /*public function notifySchool($schoolId, $title, $text, $url){
        $connection = DatabaseProvider::getDbConnection();
        $now = date('Y-m-d H:i:s');
        $usersToNotify = $connection->fetchAll("SELECT * FROM front_user WHERE fk_school_id = {$schoolId}");

        foreach ($usersToNotify as $userToNotify){
            $query = "INSERT INTO notifications SET fk_user_id = {$userToNotify['id']}, 
                        title = '{$title}', 
                        text = '{$text}', 
                        url = '{$url}',
                        created_at = '{$now}'";
            $connection->execute($query);
        }

        return true;
    }*/

    /*
     * Get newest 10 notifications for user
     */
    public function getUserNotifications($userId){
        $notifications = Notification::find([ "fk_user_id = $userId", 'limit' => 10, 'order' => 'is_read ASC, created_at DESC']);
        $host = 'http://'.$_SERVER['HTTP_HOST'];
        $notificationsData = array();
        foreach ($notifications as $notification){
            $notificationsData[] = array(
                'id' => $notification->getId(),
                'title' => $notification->getTitle(),
                'text' => $notification->getText(),
                'url' => $host.$notification->getUrl(),
                'created_at' => $notification->getCreatedAt(),
                'is_read' => $notification->getIsRead()
            );
        }

        return $notificationsData;
    }

    /*
     * Get all user notifications
     */
    public function getUserNotificationsAll($userId){
        $notifications = Notification::find([ "fk_user_id = $userId", 'order' => 'created_at DESC']);
        $host = 'http://'.$_SERVER['HTTP_HOST'];
        $notificationsData = array();
        foreach ($notifications as $notification){
            $notificationsData[] = array(
                'id' => $notification->getId(),
                'title' => $notification->getTitle(),
                'text' => $notification->getText(),
                'url' => $host.$notification->getUrl(),
                'created_at' => $notification->getCreatedAt(),
                'is_read' => $notification->getIsRead()
            );
        }

        return $notificationsData;
    }

    /*
     * Get all unread user notifications
     */
    public function getUnreadUserNotifications($userId){
        $notifications = Notification::find("fk_user_id = $userId");

        $notificationsData = array();
        foreach ($notifications as $notification){
            if($notification->getIsRead() == 0) {
                $notificationsData[] = array(
                    'id' => $notification->getId(),
                    'title' => $notification->getTitle(),
                    'text' => $notification->getText(),
                    'url' => $notification->getUrl(),
                    'created_at' => $notification->getCreatedAt(),
                    'is_read' => $notification->getIsRead()
                );
            }
        }

        return $notificationsData;
    }
}

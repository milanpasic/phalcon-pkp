<?php

/*
 * Access list by roles. Syntax is very simple:
 *
 * 'role' => [
 *     'moduleName/controllerName' => 'actions'
 * ]
 *
 * */

return [

    /* === FRONTEND - start === */
    'guest' => [
        'front/index'       => '*',
        'member/login'      => '*',
        'admin/index'       => '*',
        'api/index'         => '*',
        'public'            => '*',
        'common/product'            => '*',
    ],
    'member' => [
        'member/index'         => '*',
        'member/error'         => '*',
        'api/index'            => '*',
        'sitemap/index'        => '*',
        'login/index'          => '*',
        'member/support'       => '*',
        'member/support-send'  => '*',
        'member/notifications' => '*',
    ],
    'premium_member' => [
        /* add premium features */
    ],
    /* === FRONTEND - end === */


    /* === BACKEND - start === */
    'admin' => [
        'admin/index'                => '*',
        'file-manager/index'         => '*',
        'application/configuration'          => '*',
        'admin/admin-user'           => '*',
        'admin/front-user'           => '*',
    ],
    'super_admin' => [
        'seo/robots'       => '*',
        'seo/sitemap'      => '*',
        'seo/manager'      => '*',
        'application/language'     => '*',
        'application/translate'    => '*',
        'application/javascript'   => '*',
        /* All */
    ]
    /* === BACKEND - end === */
];
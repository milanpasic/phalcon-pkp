<?php

/* === Define global constants - START === */
define('ROOT_SERVER_PATH', '/home/pasa/projects/phalcon-pkp');
define('SUPPORT_EMAIL', 'milanpasic92@gmail.com');
define('SEND_FROM_EMAIL_USERNAME', 'email@email.com');
define('SEND_FROM_EMAIL_PASSWORD', 'voldemort');
define('PKP_URL', 'voldemort');
/* === Define global constants - END === */

return [
    'loader'         => [
        'namespaces' => [
            // Here you can setup your new vendor namespace, example:
            // 'Zend' => APPLICATION_PATH . '/../vendor/zendframework/zendframework/library/Zend',
        ],
    ],

    'assets'         => [
        'js' => [
            'static/js/library.js',
            'static/js/rotation.js',
            'static/js/main.js',

            // just comment two lines below if you don't need pages transitions via AJAX
            'vendor/history/native.history.js',
            'static/js/ajax.js',
        ],
    ],

    'admin_language' => 'sr',
];
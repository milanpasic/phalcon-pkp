<?php

/*
 * This config is needed only for devtools.
 */

return new \Phalcon\Config([
    'database'  => array(
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'root',
        'password' => 'toor',
        'dbname'   => 'phalcon-pkp',
        'charset'  => 'utf8',
    ),
    'application' => array(
        'staticUri' => '/',
        'baseUri' => '/',
        'controllersDir' => __DIR__ . '/../../app/modules/Common/Controller/',
        'modelsDir'      => __DIR__ . '/../../app/modules/Common/Model/',
        'migrationsDir'  => __DIR__ . '/../../app/migrations/',
        'viewsDir'       => __DIR__ . '/../../app/modules/Common/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
    )
]);
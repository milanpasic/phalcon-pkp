<?php

namespace Application\Mvc\Helper;

use Services\Notifications;

class NotificationHelper extends \Phalcon\Mvc\User\Component
{

    public function getNotifications()
    {
        $user = $this->session->get('auth');
        $notificationService = new Notifications();
        return $notificationService->getUserNotifications($user->id);
    }

    public function getNotificationsAll()
    {
        $user = $this->session->get('auth');
        $notificationService = new Notifications();
        return $notificationService->getUserNotificationsAll($user->id);
    }

    public function getUnreadNotifications()
    {
        $user = $this->session->get('auth');
        $notificationService = new Notifications();
        return $notificationService->getUnreadUserNotifications($user->id);
    }

}

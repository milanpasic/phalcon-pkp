<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Application\Mvc\Helper;

class RequestQuery extends \Phalcon\Mvc\User\Component
{

    public function getSymbol()
    {
        $queries = $this->request->getQuery();
        if (count($queries) == 1) {
            return '?';
        } else {
            return '&';
        }
    }

} 
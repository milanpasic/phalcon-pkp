<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Tree\Model\Translate;

use Application\Mvc\Model\Translate;

class MenuTranslate extends Translate
{

    public function getSource()
    {
        return "menu_translate";
    }

}
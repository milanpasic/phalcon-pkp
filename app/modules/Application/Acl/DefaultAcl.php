<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Application\Acl;

use Admin\Model\AdminUser;
use Phalcon\Di;

class DefaultAcl extends \Phalcon\Acl\Adapter\Memory
{

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultAction(\Phalcon\Acl::DENY);

        /**
         * Full list of Roles
         */
        $roles = [];
        $roles['guest'] = new \Phalcon\Acl\Role('guest', 'Guest');
        $roles['member'] = new \Phalcon\Acl\Role('member', 'Member');

        $roles['admin'] = new \Phalcon\Acl\Role('admin', 'Admin');
        $roles['super_admin'] = new \Phalcon\Acl\Role('super_admin', 'Super Admin');

        /**
         * Frontend roles
         */
        $this->addRole($roles['guest']);
        $this->addRole($roles['member'], $roles['guest']);

        /**
         * Backend roles
         */
        $this->addRole($roles['admin']);
        $this->addRole($roles['super_admin'], $roles['admin']);

        /**
         * Include resources permissions list from file /app/config/acl.php
         */
        $resources = include APPLICATION_PATH . '/config/acl.php';

        foreach ($resources as $roles_resources) {
            foreach ($roles_resources as $resource => $actions) {
                $registerActions = '*';
                if (is_array($actions)) {
                    $registerActions = $actions;
                }
                $this->addResource(new \Phalcon\Acl\Resource($resource), $registerActions);
            }
        }

        /**
         * Make unlimited access for admin role
         */
        $this->allow('super_admin', '*', '*');

        /**
         * Set roles permissions
         */
        foreach ($roles as $k => $role) {
            $user_resource = $resources[$k];
            foreach ($user_resource as $roles_resources => $method) {
                if ($method == '*') {
                    $this->allow($k, $roles_resources, '*');
                } else {
                    $this->allow($k, $roles_resources, $method);
                }

            }
        }

    }

    public function getAuthRole(){
        return parent::getActiveRole();
    }

    public function getHashPass(){
        $session = Di::getDefault()->get('session')->get('auth');
        return AdminUser::findFirst($session->id)->getPassword();
    }

    public function getUserId(){
        return Di::getDefault()->get('session')->get('auth')->id;
    }

}
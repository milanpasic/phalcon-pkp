<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */
namespace Member\Controller;

use Application\Mvc\Controller;
use Admin\Model\FrontUser;
use Services\MailService;
use Phalcon\Mvc\View;
use Member\Form\LoginForm;
use Member\Form\RegisterForm;

class LoginController extends Controller
{
    public function registerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $form = new RegisterForm();
        $form->initAdding();

        if ($this->request->isPost()) {
            $model = new FrontUser();
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('Welcome ', ['name' => $model->getLogin()]));
                    $this->session->set('auth', $model->getAuthData());
                    return $this->redirect('/dashboard');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
    }

    public function loginAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $form = new LoginForm();

        $domainArray = explode(".",$_SERVER['HTTP_HOST']);
        $subDomain = array_shift($domainArray);

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $login = $this->request->getPost('login', 'string');
                $password = $this->request->getPost('password', 'string');
                $user = FrontUser::findFirst("login='$login'");
                if ($user) {
                    if ($user->checkPassword($password)) {
                        if ($user->isActive()) {
                            $this->session->set('auth', $user->getAuthData());
                            return $this->redirect('/dashboard');
                        } else {
                            $this->flash->error($this->helper->translate("User is not activated yet"));
                        }
                    } else {
                        $this->flash->error($this->helper->translate("Incorrect password"));
                    }
                } else {
                    $this->flash->error($this->helper->translate("User does not exist"));
                }
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

        $this->view->demo = false;
        if($subDomain == 'demo'){
            $this->view->demo = true;
        }
        $this->view->form = $form;
    }

    public function logoutAction()
    {
        if ($this->request->isPost()) {
            $this->session->remove('auth');

        } else {
            $this->flash->error("Security errors");
        }
        $this->redirect($this->url->get());
    }

    public function demoRequestAction()
    {
        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            $mailService = new MailService();
            $body = "New Demo Request!
                Name: {$post['name']}
                Company: {$post['company']}
                Country: {$post['country']}
                Email: {$post['email']}
                Phone: {$post['tel']}";
            $mailService->send('milanpasic92@gmail.com', 'Phalcon PKP', $body);
            $this->flash->success("Demo request successfully sent. We will contact you shortly.");
        } else {
            $this->flash->error("Security errors");
        }
        $this->redirect($this->url->get());
    }
}

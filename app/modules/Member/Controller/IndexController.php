<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Member\Controller;

use Application\Mvc\Controller;
use Admin\Model\FrontUser;
use Services\DatabaseProvider;
use Services\MailService;
use Admin\Model\AdminUser;

class IndexController extends Controller
{
    public function indexAction(){
    }

    public function supportAction(){

    }

    public function supportSendAction(){
        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            if(isset($post['message']) && $post['message'] != '') {
                $mailer = new MailService();
                $mailer->send(SUPPORT_EMAIL, 'Phalcon PKP', $post['message']);
                $this->flash->success($this->helper->translate("Message sent to support"));
            }
            else{
                $this->flash->error($this->helper->translate("There's been a problem. Check config file."));
            }
        }
        $this->redirect($this->url->get());
    }

    public function profileAction(){
        $user = $this->session->get('auth');
        $this->view->user = $user;

        if(!$user->admin_session) {
            $model = FrontUser::findFirst($user->id);
        }
        else{
            $model = AdminUser::findFirst($user->id);
        }

        if (!$model) {
            $this->redirect($this->url->get() . 'index/index');
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            $model->setName($post['name']);
            $model->setEmail($post['email']);
            $user->name = $post['name'];
            $user->email = $post['email'];

            if ($this->request->hasFiles()) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $file->moveTo('public/img/profile_images/' . $user->id . '.jpg');
                }
            }

            $this->session->set('auth', $user);
            $model->save();
            $model->refresh();

            $this->redirect('/profile');
        }
        $fileTime = filemtime(ROOT_SERVER_PATH . '/public/img/profile_images/' . $user->id . '.jpg');
        $this->view->fileTime = $fileTime;
    }

    public function setReadNotificationAction(){
        if ($this->request->isPost()) {
            $user = $this->session->get('auth');
            // Demo User = 5
            if($user->id != 5) {
                $post = $this->request->getPost();
                $notificationId = $post['id'];

                /* Should be reffactored */
                $connection = DatabaseProvider::getDbConnection();

                $query = "UPDATE notifications SET is_read = 1 WHERE id = {$notificationId}";
                $connection->execute($query);
            }
            return true;
        }
    }

    public function route404Action(){

    }

    public function notificationsAction(){
        $this->view->user_id = $this->session->get('auth')->id;
    }
}

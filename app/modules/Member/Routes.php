<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Member;

class Routes
{
    public function init($router)
    {
        $router->addML('/dashboard', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'index',
        ), 'dashboard');

        $router->addML('/profile', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'profile',
        ), 'profile');

        $router->addML('/support', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'support',
        ), 'support');

        $router->addML('/support-send', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'supportSend',
        ), 'support-send');

        $router->addML('/set-read-notification', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'setReadNotification',
        ), 'set-read-notification');

        $router->addML('/notifications', array(
            'module' => 'member',
            'controller' => 'index',
            'action' => 'notifications',
        ), 'notifications');

        $router->addML('/register', array(
            'module' => 'member',
            'controller' => 'login',
            'action' => 'register',
        ), 'register');

        $router->addML('/login', array(
            'module' => 'member',
            'controller' => 'login',
            'action' => 'login',
        ), 'login');

        $router->addML('/demo-request', array(
            'module' => 'member',
            'controller' => 'login',
            'action' => 'demoRequest',
        ), 'demo-request');

        //Set 404 paths
        $router->notFound(array(
            'module' => 'member',
            "controller" => "index",
            "action" => "route404"
        ));

        return $router;
    }
}
<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Member\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Email as ValidatorEmail;
use Phalcon\Validation\Validator\PresenceOf;

class RegisterForm extends Form
{

    public function initialize()
    {
        $this->add(
            (new Text('login', [
                'required' => true,
                'placeholder' => 'Login nickname',
            ]))->setLabel('Login')
        );

        $this->add(
            (new Email('email', [
                'required' => true,
                'placeholder' => 'Email',
            ]))
                ->addValidator(new ValidatorEmail([
                    'message' => 'Email format is invalid',
                ]))
                ->setLabel('Email')
        );

        $this->add(
            (new Text('name', [
                'required' => true,
                'placeholder' => 'Name',
            ]))
                ->setLabel('Name')
        );

        $this->add(
            (new Password('password', [
                'required' => true,
                'placeholder' => 'Password',
            ]))
                ->setLabel('Password')
        );

        $this->add((new Hidden('role'))->setAttribute('value', 'member'));

        $this->add((new Hidden('active'))->setAttribute('value', '1'));
    }

    public function initAdding()
    {
        $password = $this->get('password');
        $password->setAttribute('required', true);
        $password->addValidator(new PresenceOf([
            'message' => 'Password is required',
        ]));

    }

}

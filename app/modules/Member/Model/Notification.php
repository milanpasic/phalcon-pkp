<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Member\Model;

use Application\Mvc\Model\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Application\Localization\Transliterator;

class Notification extends Model
{

    public function getSource()
    {
        return "notifications";
    }

    public $id;
    public $fk_user_id;
    public $title;
    public $text;
    public $url;
    public $is_read;
    public $created_at;

    public function initialize()
    {
        // Notification belongs to One User
        $this->belongsTo('fk_user_id', 'Admin\Model\FrontUser', 'id', [
            'alias' => 'user'
        ]);

        $this->created_at = date('Y-m-d H:i:s');
    }

    public function beforeCreate()
    {

    }


    public function validation()
    {
        $validator = new Validation();
        return $this->validate($validator);
    }

    public function updateFields($data)
    {
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * @param mixed $fk_user_id
     */
    public function setFkUserId($fk_user_id)
    {
        $this->fk_user_id = $fk_user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFkUserId()
    {
        return $this->fk_user_id;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setIsRead($isRead)
    {
        $this->is_read = $isRead;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsRead()
    {
        return $this->is_read;
    }
}

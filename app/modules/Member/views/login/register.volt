<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Phalcon PKP</title>

    <link href="{{ url.path() }}vendor/semantic-2.1/semantic.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/login.css" rel="stylesheet" type="text/css">

    <script src="{{ url.path() }}vendor/js/jquery-1.11.0.min.js"></script>
    <script src="{{ url.path() }}vendor/js/html5shiv.js"></script>
    <style>
        .container {
            width: 400px;
            margin: 100px auto 0;
        }
    </style>
</head>
<body>
<div id="login-page">
    <div class="container">
        <form class="ui form segment login_form" method="post" action="/member/login/register">
            <div id="title-holder">Phalcon PKP Member Panel</div>
            <div class="required field">
                <div class="ui icon input">
                    <i class="login-icon">&nbsp;</i>
                    {{ form.render('name') }}
                </div>
            </div>

            <div class="required field">
                <div class="ui icon input">
                    <i class="login-icon">&nbsp;</i>
                    {{ form.render('login') }}
                </div>
            </div>

            <div class="required field">
                <div class="ui icon input">
                    <i class="login-icon">&nbsp;</i>
                    {{ form.render('email') }}
                </div>
            </div>

            <div class="required field">
                <div class="ui icon input">
                    <i class="password-icon"></i>
                    {{ form.render('password') }}
                </div>
            </div>

            {{ form.renderDecorated('role') }}
            {{ form.renderDecorated('active') }}

            {{ flash.output() }}

            <input type="submit" id="submit" class="ui blue submit button" value="{{ helper.translate('Register') | upper }}">
        </form>
    </div>
</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Phalcon PKP</title>

    <link href="{{ url.path() }}vendor/semantic-2.1/semantic.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/login.css" rel="stylesheet" type="text/css">

    <script src="{{ url.path() }}vendor/js/jquery-1.11.0.min.js"></script>
    <script src="{{ url.path() }}vendor/js/html5shiv.js"></script>
    <style>
        .container {
            width: 400px;
            margin: 100px auto 0;
        }
    </style>
</head>
<body>
<div id="login-page">
    <div class="container">
        <form class="ui form segment login_form" method="post" action="/member/login/login">
            <div id="title-holder">Phalcon PKP Member Panel</div>
            <div class="required field">
                <div class="ui icon input">
                    <i class="login-icon">&nbsp;</i>
                    {{ form.render('login') }}
                </div>
            </div>
            <div class="required field">
                <div class="ui icon input">
                    <i class="password-icon"></i>
                    {{ form.render('password') }}
                </div>
            </div>
            {{ flash.output() }}
            <input type="submit" id="submit" class="ui blue submit button" value="{{ helper.translate('Log In') | upper }}">
            {% if demo %}
                <input type="button" id="free_demo" class="ui blue submit button" value="{{ helper.translate('Request Free Demo') | upper }}">
            {% endif %}
        </form>

        {% if demo %}
        <div id="request_demo_form">
            <form class="ui form segment login_form" method="post" action="/loin/igndex/demo-request">
                <div class="required field">
                    <div class="ui icon input">
                        <input type="text" id="name" class="normal" name="name" required="1" placeholder="{{ helper.translate('Your name...') }}">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui icon input">
                        <input type="text" id="company" class="normal" name="company" required="1" placeholder="{{ helper.translate('Your company...') }}">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui icon input">
                        <input type="text" id="country" class="normal" name="country" required="1" placeholder="{{ helper.translate('Your country...') }}">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui icon input">
                        <input type="email" id="email" name="email" required="1" placeholder="{{ helper.translate('Your email...') }}">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui icon input">
                        <input type="tel" id="tel" name="tel" required="1" placeholder="{{ helper.translate('Your phone...') }}">
                    </div>
                </div>
                {{ flash.output() }}
                <input type="submit" id="submit" class="ui blue submit button" value="{{ helper.translate('Send Request') | upper }}">
            </form>
        </div>
        {% endif %}
        <a class="site_changer" href="/admin">ADMIN PANEL</a>
    </div>
</div>

{% if demo %}
<script>
    $(document).ready(function () {
        $('#request_demo_form').hide();
        $('#free_demo').click(function (e) {
            e.preventDefault();
            $('#request_demo_form').slideToggle('slow', function () {
                if($('#request_demo_form').is(':visible')){
                    $('#login-page').css('height', 'calc(100% + 452px)');
                }
                else{
                    $('#login-page').css('height', 'calc(100% + 100px)');
                }
            });
        });
    });
</script>
{% endif %}

</body>
</html>
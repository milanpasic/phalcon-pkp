<div class="row t_center title blue_font">
    {{ helper.translate('Notifications') }}
</div>

<div class="row t_center subtitle">
    {{ helper.translate('notifications minor message') }}
</div>

<div class="publications list">

    {% if helper.notificationsDataAllCount() != 0 %}
        {% for notification in helper.notificationsDataAll() %}
            <div class="item_list">
                <div class="col-sm-3 list_title {% if notification['is_read'] == 0 %}not_read{% endif %}">{{ helper.announce(notification['title'], 20) }}</div>
                <div class="col-sm-6 list_description">{{ helper.announce(notification['text'], 60) }}</div>
                <div class="col-sm-2 list_button">
                    <a class="btn btn-lk btn-lk-blue" href="{{ notification['url'] }}">{{ helper.translate('view') }}</a>
                </div>
            </div>
        {% endfor %}
    {% else %}
        <p>{{ helper.translate('Entries not found') }}</p>
    {% endif %}

</div>


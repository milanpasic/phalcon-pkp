{{ flash.output() }}
<div class="row t_center title blue_font">
{{ helper.translate('dashboard major message') }}
</div>

<div class="row t_center subtitle">
{{ helper.translate('dashboard minor message') }}
</div>
<div class="clearfix"></div>

Phalcon PKP

{#
<div class="row t_center blue_font buttons_holder">
    <div class="col-md-3 col-fix">
        <div class="landing_button">
            <div class="row image"><img src="{{ url.path() }}static/images/course_home.png"/></div>
            <div class="row landing_title">{{ helper.translate('Courses') | upper }}</div>
            <div class="row description">{{ helper.translate('Courses Landing Description') }}</div>
            <div class="row button_holder">
                {{ helper.menu.item( helper.translate('Courses'), 'courses', helper.langUrl(['for':'publications', 'type':'courses']), ['a': ['class' : 'btn btn-lk btn-lk-main btn-main-fix']] ) }}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-fix">
        <div class="landing_button">
            <div class="row image"><img src="{{ url.path() }}static/images/notification_home.png"/></div>
            <div class="row landing_title">{{ helper.translate('News') | upper }}</div>
            <div class="row description">{{ helper.translate('Announcements Landing Description') }}</div>
            <div class="row button_holder">
                {{ helper.menu.item( helper.translate('News'), 'news', helper.langUrl(['for':'publications', 'type':'news']), ['a': ['class' : 'btn btn-lk btn-lk-main btn-main-fix']] ) }}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-fix">
        <div class="landing_button">
            <div class="row image"><img src="{{ url.path() }}static/images/test_home.png"/></div>
            <div class="row landing_title">{{ helper.translate('Testing Platform') | upper }}</div>
            <div class="row description">{{ helper.translate('Exams Landing Description') }}</div>
            <div class="row button_holder">
                <a class="btn btn-lk btn-lk-main btn-main-fix" href="http://{{ constant('LEARNING_KEY_TESTING_URL') }}">{{ helper.translate('Testing Platform') }}</a>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-fix">
        <div class="landing_button">
            <div class="row image"><img src="{{ url.path() }}static/images/analytics_home.png"/></div>
            <div class="row landing_title">{{ helper.translate('Analytics') | upper }}</div>
            <div class="row description">{{ helper.translate('Analytics Landing Description') }}</div>
            <div class="row button_holder">
                {{ helper.menu.item( helper.translate('Analytics'), 'analytics', helper.langUrl(['for':'analytics']), ['a': ['class' : 'btn btn-lk btn-lk-main btn-main-fix']] ) }}
            </div>
        </div>
    </div>
</div>#}

<link href="{{ url.path() }}vendor/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="{{ url.path() }}vendor/js/fileinput.js" type="text/javascript"></script>

<div class="row t_center title blue_font">
    {{ helper.translate('Edit Profile') }}
</div>

<div class="row t_center subtitle">
    {{ helper.translate('profile minor message') }}
</div>

<div class="landing_main">
    <div class="row">
        <form class="text-center" method="post" enctype="multipart/form-data">

            <div class="col-md-4">
                <div class="form-group">
                    <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none"></div>
                    <div class="kv-avatar center-block" style="width:200px">
                        <label for="avatar">{{ helper.translate('Profile Photo') }}</label>
                        <input id="avatar" name="avatar" type="file" class="file-loading">
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">{{ helper.translate('Email Address') }}</label>
                    <input type="email" name="email" class="form-control lk-text-small" id="email" required aria-describedby="emailHelp" value="{{ user.email }}" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="name">{{ helper.translate('First and Last name') }}</label>
                    <input type="text" name="name" class="form-control lk-text-small" id="name" required aria-describedby="nameHelp" value="{{ user.name }}" placeholder="Enter Name">
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-lk btn-lk-form" value="{{ helper.translate('Update') }}">
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#avatar").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '{{ helper.translate('Browse') }}',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-camera"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: '{{ helper.translate('Cancel changes') }}',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="{{ url.path() }}img/profile_images/{{ user.id }}.jpg?{{ fileTime }}" alt="Your Avatar" style="width:220px">',
            layoutTemplates: {main2: '{preview} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
    });
</script>
<div class="row t_center title blue_font">
    {{ helper.translate('Support') }}
</div>

<div class="row t_center subtitle">
    {{ helper.translate('support minor message') }}
</div>

<div class="landing_main">
    <form method="post" action="/support-send">

        <div class="form-group">
            <label for="type">{{ helper.translate('Select Type of Problem:') }}</label>
            <select name="type" class="form-control lk-select-small" id="type">
                <option>Main Platform</option>
            </select>
        </div>

        <div class="form-group">
            <label for="message">{{ helper.translate('Message') }}</label>
            <textarea name="message" class="form-control lk-textarea" rows="8" id="message"></textarea>
        </div>

        <button type="submit" class="btn btn-lk btn-lk-form">{{ helper.translate('Submit') }}</button>
    </form>
</div>
<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Admin;

class Routes
{

    public function init($router)
    {
        $router->add('/admin', array(
            'module'     => 'admin',
            'controller' => 'index',
            'action'     => 'index',
        ))->setName('admin');

        $router->add('/delete-file', array(
            'module'     => 'admin',
            'controller' => 'index',
            'action'     => 'deleteFile',
        ))->setName('delete-file');

        return $router;
    }

}
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>

        <link href="{{ url.path() }}vendor/semantic-2.1/semantic.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url.path() }}static/css/login.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="{{ url.path() }}vendor/js/html5shiv.js"></script>
            <script src="{{ url.path() }}vendor/js/respond.min.js"></script>
        <![endif]-->
        <style>
        .container {
            width: 400px;
            margin: 100px auto 0;
        }
        </style>
    </head>
    <body>
        <div id="login-page">
            <div class="container login_form">
                <form class="ui form segment login_form" method="post" action="{{ url.get() }}admin/index/login">
                    <div id="title-holder">Phalcon PKP Admin Panel</div>
                    <div class="required field">
                        <div class="ui icon input">
                            <i class="login-icon">&nbsp;</i>
                            {{ form.render('login') }}
                        </div>
                    </div>
                    <div class="required field">
                        <div class="ui icon input">
                            <i class="password-icon"></i>
                            {{ form.render('password') }}
                        </div>
                    </div>
                    {{ flash.output() }}
                    <input type="hidden" name="{{ security.getTokenKey() }}"
                           value="{{ security.getToken() }}"/>
                    <input type="submit" id="submit" class="ui blue submit button" value="{{ helper.translate('Log In') | upper }}">
                </form>
            </div>
        </div>
        <a class="site_changer" href="/login">MAIN PLATFORM</a>
    </body>
</html>

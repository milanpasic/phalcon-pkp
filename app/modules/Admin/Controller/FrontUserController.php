<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Admin\Controller;

use Application\Mvc\Controller;
use Admin\Form\FrontUserForm;
use Admin\Model\FrontUser;

class FrontUserController extends Controller
{

    public function initialize()
    {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('front-user');

        $this->view->languages_disabled = true;
    }

    public function indexAction()
    {
        $this->view->entries = FrontUser::find([
            "order" => "id DESC"
        ]);

        $this->helper->title($this->helper->at('Manage Users'), true);
    }

    public function addAction()
    {
        $this->view->pick(['front-user/edit']);

        $model = new FrontUser();
        $form = new FrontUserForm();
        $form->initAdding();

        if ($this->request->isPost()) {
            $model = new FrontUser();
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('User created', ['name' => $model->getLogin()]));
                    $this->redirect($this->url->get() . 'admin/front-user');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Administrator'), true);
    }

    public function editAction($id)
    {
        $model = FrontUser::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'admin/front-user');
        }

        $form = new FrontUserForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    $this->flash->success('User <b>' . $model->getLogin() . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'admin/front-user');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Users'), true);
    }

    public function deleteAction($id)
    {
        $model = FrontUser::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'admin/front-user');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting student <b>' . $model->getLogin() . '</b>');
            return $this->redirect($this->url->get() . 'admin/front-user');
        }

        $this->view->model = $model;
        $this->helper->title($this->helper->at('Delete User'), true);
    }
}

<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Front;

class Routes
{
    public function init($router)
    {
        $router->addML('/', array(
            'module' => 'front',
            'controller' => 'index',
            'action' => 'index',
        ), 'index');

        return $router;
    }
}
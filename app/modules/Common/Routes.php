<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Common;

class Routes
{
    public function init($router)
    {
        $router->add(
            "/:module/:controller/:action/:params",
            [
                "module"     => 1,
                "controller" => 2,
                "action"     => 3,
                "params"     => 4,
            ]
        );

        return $router;
    }
}
<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Common\Controller;

use Application\Mvc\Controller;

class CommonController extends Controller
{
    public function indexAction(){

    }
}

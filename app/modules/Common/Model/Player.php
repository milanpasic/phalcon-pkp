<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Common\Model;

use Admin\Model\FrontUser;
use Traits\Timestamp;

class Player extends BaseModel
{
    use Timestamp;

    public function getSource()
    {
        return "player";
    }

    public $id;
    public $name;
    public $created_by;
    public $created_at;
    public $updated_at;

    public function initialize()
    {
        // shipment belongs to One User
        $this->belongsTo('created_by', 'Admin\Model\FrontUser', 'id', [
            'alias' => 'user'
        ]);
    }

    public static function get($currentPage, $limit, $parameters = null){
        if(!$currentPage){
            $currentPage = 1;
        }

        $start = $currentPage * $limit - $limit;
        $end = $limit;

        if(!is_null($parameters)) {
            $parameters = array_merge([$parameters], array('limit' => array('number' => $end, 'offset' => $start)));
        }
        else{
            $parameters = array('limit' => array('number' => $limit, 'offset' => $start));
        }

        $results = parent::find($parameters);
        $resultsArray = $results->toArray();

        foreach ($resultsArray as $key => $item){
            $resultsArray[$key]['owner'] = $results[$key]->user;
        }

        $total = parent::find()->count();
        $totalPages = ceil($total / $limit);

        $response = [
            "items" => $resultsArray,
            "first" => 1,
            "before" => $currentPage == 1 ? 1 : $currentPage - 1,
            "current" => (int) $currentPage,
            "last" => $totalPages,
            "next" => $currentPage == $totalPages ? $currentPage : $currentPage + 1,
            "total_pages" => $totalPages,
            "total_items" => $total,
            "limit" => $limit
        ];

        return $response;
    }
}
<?php

namespace Common\Model;

class BaseModel extends \Phalcon\Mvc\Model
{
    public function __get($name)
    {
        $attributeValue = parent::__get($name);
        if(!$attributeValue) {
            if(is_array($attributeValue)) {
                return [];
            }
            return null;
        }
        return $attributeValue;
    }
}
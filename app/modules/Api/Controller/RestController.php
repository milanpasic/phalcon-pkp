<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Api\Controller;

use Admin\Model\FrontUser;
use Phalcon\Di;
use Phalcon\Dispatcher;

/**
 * Class RestController.
 * Base class for all controllers for REST API.
 *
 * @package Api\Controller
 */
class RestController extends \Application\Mvc\Controller
{
    protected $user;
    protected $params;

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        if(!($dispatcher->getControllerName() == 'auth' && $dispatcher->getActionName() == 'login')) {
            //$this->authorize();
        }

        $json = [];
        if($this->request->getContentType() == 'application/json') {
            $jsonParams = json_decode(json_encode($this->request->getJsonRawBody()), true);
            if(is_array($jsonParams)){
                $json = $jsonParams;
            }
        }
        $body = array_merge(
            $this->request->getPut(),
            $this->request->getPost(),
            $this->request->getQuery()
        );

        $this->params = array_merge($json, $body);
    }

    /**
     * Render data from payload
     *
     * @throws \Api\Exception\NotImplementedException
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    protected function render(\Api\Model\Payload $payload)
    {
        $format = $this->request->getQuery('format', null, 'json');

        switch ($format)
        {
            case 'json':
                $contentType = 'application/json';
                $encoding = 'UTF-8';
                $content = json_encode($payload->toArray());
                break;
            default:
                throw new \Api\Exception\NotImplementedException(
                    sprintf('Requested format %s is not supported yet.', $format)
                );
                break;
        }

        if(!$payload->error) {
            $this->response->setStatusCode(200);
        }
        else{
            $this->response->setStatusCode(400);
        }
        $this->response->setContentType($contentType, $encoding);
        $this->response->setContent($content);

        return $this->response->send();
    }

    protected function response($data){
        $this->render(new \Api\Model\Payload($data));
    }

    protected function error($data){
        $this->render(new \Api\Model\Payload($data, true));
        exit;
    }

    private function authorize()
    {
        $authorizationHeader = $this->request->getHeader('Authorization');
        $token = str_replace('Bearer ', '', $authorizationHeader);
        if ($token) {
            $user = FrontUser::findFirst("access_token='" . $token . "'");
            if($user){
                $this->user = $user;
            }
        }
        if(!$this->user) {
            $this->error('Unauthorized');
        }
    }
}
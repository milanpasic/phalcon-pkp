<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Api\Controller;

/**
 * Class IndexController.
 *
 * @package Api\Controller
 */
class IndexController extends \Api\Controller\RestController
{
    /**
     * API start page.
     *
     * @throws \Api\Exception\NotImplementedException
     */
    public function indexAction()
    {
        $payload = new \Api\Model\Payload('Welcome to api for phalcon-pkp!');

        $this->render($payload);
    }

    public function pasaAction()
    {
        $this->response('asdas');
    }
}

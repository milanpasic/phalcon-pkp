<?php

/**
 * @copyright Copyright (c) 2017 Milan Pasic (https://github.com/milanpasic92)
 * @author Milan Pasic <milanpasic92@gmail.com>
 */

namespace Api;

use Phalcon\Mvc\Router\Group as RouterGroup;

class Routes
{

    public function init($router)
    {
        $api = new RouterGroup(
            [
                'module'     => 'api',
            ]
        );
        $api->setPrefix('/api');

        $api->addGet('/', array(
            'controller' => 'index',
            'action'     => 'index',
        ))->setName('api');

        $api->addGet('/pasa', array(
            'controller' => 'index',
            'action'     => 'pasa',
        ))->setName('index_pasa');

        $router->mount($api);
        return $router;
    }
}

<div class="ui left fixed vertical pointing inverted menu">
    <a class="item{{ helper.activeMenu().activeClass('admin-home') }} header" href="{{ url(['for': 'admin']) }}">
        Phalcon PKP SuperAdmin
    </a>
    <div class="item">
        <div class="header">{{ helper.at('Contents') }} <i class="font icon"></i></div>
    </div>

    <div class="item">
        <div class="header">{{ helper.at('Admin') }} <i class="wrench icon"></i></div>

        <div class="menu">
            <a class="item{{ helper.activeMenu().activeClass('admin-user') }}" href="{{ url.get() }}admin/admin-user">
                {{ helper.at('Manage Admins') }} <i class="user icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('admin-cms') }}" href="{{ url.get() }}application/configuration">
                {{ helper.at('Configuration') }} <i class="settings icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('admin-language') }}" href="{{ url.get() }}application/language">
                {{ helper.at('Languages') }} <i class="globe icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('admin-translate') }}" href="{{ url.get() }}application/translate">
                {{ helper.at('Translate') }} <i class="book icon"></i>
            </a>
        </div>
    </div>
    <div class="item">
        <div class="header">{{ helper.at('Users') }} <i class="wrench icon"></i></div>

        <div class="menu">
            <a class="item{{ helper.activeMenu().activeClass('admin-user') }}" href="{{ url.get() }}admin/front-user">
                {{ helper.at('Manage Users') }} <i class="user icon"></i>
            </a>
        </div>
    </div>
    <div class="item">
        <a href="{{ url.get() }}" class="ui primary tiny button" target="_blank">
            <i class="home icon"></i>{{ helper.at('View Site') }}
        </a>
        <br><br>
        <a href="javascript:void(0);" class="ui tiny button" onclick="document.getElementById('logout-form').submit()">
            <i class="plane icon"></i>{{ helper.at('Logout') }}
        </a>
        <br><br>
        <form action="{{ url.get() }}admin/index/logout" method="post" style="display: none;" id="logout-form">
            <input type="hidden" name="{{ security.getTokenKey() }}"
                   value="{{ security.getToken() }}">
        </form>
    </div>
</div>
{% if helper.getRole() == 'super_admin' %}
{{ partial('admin/navs/super_admin') }}
{% endif %}

{% if helper.getRole() == 'admin' %}
{{ partial('admin/navs/admin') }}
{% endif %}
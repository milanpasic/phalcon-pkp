<div id="footer-content">
    Copyright @ 2017 <a href="http://{{ constant('PKP_URL') }}/" target="_blank">PKP Innovations</a>
</div>

{# === Mobile / Desktop version switcher === #}
{#
<a class="device-version noajax"
   href="{{ url.get() }}?mobile={% if constant('MOBILE_DEVICE') == true %}false{% else %}true{% endif %}">
    {% if constant('MOBILE_DEVICE') == true %}{{ helper.translate('Full Version') }}{% else %}{{ helper.translate('Mobile version') }}{% endif %}
</a>
#}

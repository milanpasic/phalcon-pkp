{#{{ helper.staticWidget('title_in_header') }}#}
{# lan menu #}
<div id="header-top">
    {% set languages = helper.languages() %}
    {% if languages|length > 1 %}
        <div id="languages-holder">
            {% for language in languages %}
                <div class="lang">
                    {{ helper.langSwitcher(language['iso'], language['short_name'] | upper) }}
                </div>
                {% if not loop.last %}
                    <span class="lang-divider">/</span>
                {% endif %}
            {% endfor %}
        </div>
    {% endif %}

    <ul id="menu-front">
        {{ helper.menu.item( helper.translate('Home') | upper, 'index', helper.langUrl(['for':'index']), ['a': ['class' : 'noajax']] ) }}
        {{ helper.menu.item( helper.translate('Main Platform') | upper, 'dashboard', helper.langUrl(['for':'dashboard']), ['a': ['class' : 'noajax']] ) }}
        {{ helper.menu.item( helper.translate('Register On Main Platform') | upper, 'register', helper.langUrl(['for':'register']), ['a': ['class' : 'noajax']] ) }}
        {{ helper.menu.item( helper.translate('Admin') | upper, null, url(['for':'admin']), ['li':['class':'last'], 'a':['class':'noajax']] ) }}
        {#
            submenu items exampple:

            {{ helper.menu.item( 'Services', 'services', langUrl(['for':'services']), [],
            [
                helper.menu.item( 'Printing', 'printing', langUrl(['for':'printing']) ),
                helper.menu.item( 'Design', 'design', langUrl(['for':'design']) )
            ]
            ) }}

        #}
    </ul>
</div>
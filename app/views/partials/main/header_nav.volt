<div id="header-nav">
    <ul id="menu">
        {{ helper.menu.item( helper.translate('Dashboard') | upper, 'dashboard', helper.langUrl(['for':'dashboard']), ['a': ['class' : 'noajax']] ) }}
        {{ helper.menu.item( helper.translate('Support') | upper, 'support', helper.langUrl(['for':'support']), ['a': ['class' : 'noajax']] ) }}
    </ul>
    <div id="right-nav">
        <div class="f_left right-nav-icon" id="notifications">
            {% if helper.notificationsCount() != 0 %}
            <span id="notification-counter">{{ helper.notificationsCount() }}</span>
            {% endif %}
            <div id="notifications_holder" style="display: none">
                {% if helper.notificationsDataAllCount() == 0 %}
                <div id="no_notificaitons">{{ helper.translate('You do not have unread notifications.') }}</div>
                {% else %}
                <div id="notifications_wrapper">
                    {% for notification in helper.notificationsData() %}
                        <div class="single_notification {% if notification['is_read'] == 0 %}not_read{% endif %}">
                            <a data-id="{{ notification['id'] }}" href="{{ notification['url'] }}" class="notification_title">{{ notification['title'] }}</a>
                            <div class="notification_text">{{ notification['title'] }}</div>
                        </div>
                    {% endfor %}
                    <a href="/notifications" id="view_all_notification">{{ helper.translate('View All') }}</a>
                </div>
                {% endif %}
            </div>
        </div>
        <span class="f_left vr">&nbsp;</span>
        <div class="f_left right-nav-icon" id="profile-image-holder">
            <a href="{{ helper.langUrl(['for':'profile']) }}"><img src="{{ helper.profileImage() }}"></a>
            <span>.</span>
        </div>
    </div>
</div>
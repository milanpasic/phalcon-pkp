{#{{ helper.staticWidget('title_in_header') }}#}
{# lan menu #}
<div id="header-top">
    {% set languages = helper.languages() %}
    {% if languages|length > 1 %}
        <div id="languages-holder">
            {% for language in languages %}
                <div class="lang">
                    {{ helper.langSwitcher(language['iso'], language['short_name'] | upper) }}
                </div>
                {% if not loop.last %}
                    <span class="lang-divider">/</span>
                {% endif %}
            {% endfor %}
        </div>
    {% endif %}


    {# logout button #}
    <a id="logout-button" href="javascript:void(0);" class="ui tiny button" onclick="document.getElementById('logout-form').submit()">
        {{ helper.at('Logout') | upper }} <i class="glyphicon glyphicon-log-out"></i>
    </a>
    <form action="{{ url.get() }}member/login/logout" method="post" style="display: none;" id="logout-form">
        <input type="hidden" name="{{ security.getTokenKey() }}"
               value="{{ security.getToken() }}">
    </form>
</div>
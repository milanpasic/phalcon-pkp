<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Phalcon PKP</title>

    {{ helper.meta().get('description') }}
    {{ helper.meta().get('keywords') }}
    {{ helper.meta().get('seo-manager') }}

    <link href="{{ url.path() }}favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">

    <!--css reset-->
    <link href="{{ url.path() }}vendor/css/reset.min.css" rel="stylesheet" type="text/css">
    <!--/css reset -->

    <!--css libs-->
    <link href="{{ url.path() }}vendor/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}vendor/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/main.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/header.css" rel="stylesheet" type="text/css">
    <link href="{{ url.path() }}static/css/footer.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!--/css libs-->

    <!--less-->
    <link href="{{ url.path() }}static/less/style.less" rel="stylesheet/less" type="text/css">
    <script src="{{ url.path() }}vendor/js/less-1.7.3.min.js" type="text/javascript"></script>
    <!--/less-->

    <script src="{{ url.path() }}vendor/js/jquery-1.11.0.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/vendor/js/html5shiv.js"></script>
    <script src="/vendor/js/respond.min.js"></script>
    <![endif]-->

    <script src="{{ url.path() }}static/js/ui.js"></script>

    <!--js-->
    {{ assets.outputJs('js') }}
    <!--/js-->
</head>
<body{% if view.bodyClass %} class="{{ view.bodyClass }}"{% endif %}>

<div id="wrapper">
    {{ content() }}
</div>

</body>
</html>
<div class="wrapper-in">
    <header>
        {{ partial('main/header_top') }}
        {{ partial('main/header_cover') }}
        {{ partial('main/header_nav') }}
    </header>

    <div id="main" class="container">

        {{ content() }}

        {% if seo_text is defined and seo_text_inner is not defined %}
            <div class="seo-text">
                {{ seo_text }}
            </div>
        {% endif %}

    </div>

    <footer>
        {{ partial('main/footer') }}
    </footer>

</div>

{% if registry.cms['PROFILER'] %}
    {{ helper.dbProfiler() }}
{% endif %}
<div class="wrapper-in">
    <header>
        {{ partial('front/header') }}
    </header>

    <div id="main" class="container">

        {{ content() }}

        {% if seo_text is defined and seo_text_inner is not defined %}
            <div class="seo-text">
                {{ seo_text }}
            </div>
        {% endif %}

    </div>

    <footer>
        {{ partial('front/footer') }}
    </footer>

</div>

{% if registry.cms['PROFILER'] %}
    {{ helper.dbProfiler() }}
{% endif %}
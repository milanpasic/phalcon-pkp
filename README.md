# Phalcon PKP

## Overview

Phalcon is ridiculously fast. I’ve been using it since it came out. The documentation used to be a bit spotty, however, has vastly improved over the past 2 years.

I wouldn’t say its for beginners though - so if you’re just starting out, perhaps its not for you.

Included Packages:

~~~
jakub-onderka/php-console-color (0.1)
kartik-v/bootstrap-fileinput (dev-master 6c4be4f)
mandrill/mandrill (1.0.55)
michelf/php-markdown (1.7.0)
mobiledetect/mobiledetectlib (2.8.25)
swiftmailer/swiftmailer (v5.4.7)
phalcon-ext/mailer (v2.0.3)
symfony/var-dumper (v3.2.8)
psr/log (1.0.2)
symfony/debug (v3.2.8)
nikic/php-parser (v3.0.5)
jakub-onderka/php-console-highlighter (v0.3.2)
dnoegel/php-xdg-base-dir (0.1)
psy/psysh (v0.8.3)
phalcon/devtools (dev-master a832aef)
symfony/yaml (v3.2.8)
symfony/filesystem (v3.2.8)
symfony/config (v3.2.8)
robmorgan/phinx (dev-master 5e6d17d)
sneakybobito/phalcon-stubs (v3.0.1)
~~~

## Install Guide

### Please don't copy-paste commands blindly! 
### Read this carefully and try to understand every step because (trust me) you are gonna need that!

#### This tutorial is for Ubuntu ^14.xx / ^Apache 2.x / ^MySQL 5.x / PHP ^7.x
#### It's been originally created and tested on: 
```
Ubuntu: 16.10
PHP: 7.0.20-1
Apache: 2.4.18
MySQL: 5.7.18
``` 

Let's start! Clone the repository:
```
git clone https://bitbucket.org/milanpasic/phalcon-pkp.git 
cd phalcon-pkp
```
Configure your virtual host..

Install php 7.x

Add ppa:ondrey repository <- this repository is gold mine! Long live  Ondřej Surý!!!
```
sudo apt-add-repository ppa:ondrej/php
sudo apt-get update
```

Install Phalcon Dependencies:
```
sudo apt-get install php7.0-phalcon
```

Run composer dry run to see what is included:
```
sudo composer install --dry-run
```

If all is ok with you, run:
```
sudo composer install
```

Create symbolic link for phalcon devtools. You don't need to compile/install it.. It's already pulled trough dependencies:
```
sudo ln -s vendor/phalcon/devtools/phalcon.php phalcon
```

We are almost there :) 

Run phalcon devtools:
```
php phalcon
```

There are a lot of VERY handy and useful tools.. for now, we just need migration tool:
```
php phalcon migration run
```

You'll see:
```
Phalcon DevTools (3.1.2)

ERROR: SQLSTATE[HY000] [1049] Unknown database 'phalcon-pkp'
```

So, let's create and configure database connection:

Go to app/config/config.php and setup connection to your database.

Note that this is only for dev tools!!! The framework is built to work with multiple environments. You'll find those setups in app/config/environment directory and edit those as well for your environments.

After that run migration again:
```
php phalcon migration run
```

It should say that all versions of migrations are migrated. :)

##### Congratulations! You are officially flying on phalcon now :)

Try to open the site you configured in virtual host.

if it says something like:
```
Fatal error: Uncaught Error: Class 'Phalcon\DI\FactoryDefault' not found in
```

It means that you have been naughty a bit and probably manually changed php ini file.. Find out which one is your main one and add phalcon php extension. It's probably this one:
```
/etc/php/7.0/apache2/php.ini
```

You need to add this on the end of that file:
```
extension=phalcon.so
```

Great :) Now we just need to enable write access for runtime directories:

```
sudo chmod 777 -R data/cache/
sudo chmod 777 -R public/assets/
```

### Congrats!

ps: default admin user credentials are: super_admin:voldemort.. You can't do anything without that info actually :D

#####Regards!
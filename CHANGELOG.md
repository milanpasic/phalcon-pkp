# Phalcon PKP CHANGELOG
> Phalcon PKP by Milan Pasic - PKP Inovations

### v0.3 - Release
> 2017-05-04
- Code cleaned up
- Frontend permissions system
- Dashboard initial wireframe
- A bunch of minor bugs fixed
- Dependencies fixes - major bug fixed
- Languages support
- Notifications feature
- Style fixes
- Fixed announcement helper

#### v0.2 - minorities
> 2017-02-10
- Landing Page
- Design Updates
- Architecture improvements
- Support features
- Support mails setup


## v0.1 Phalcon PKP Initialized!
> 2016-11-23
- Repository created
- Started Changelog
- Dependencies loaded
- Architecture setup